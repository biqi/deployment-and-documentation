# Deployment and Documentation

User guides and example deployments for the Tracker Online Software stack.

## Contents
[[_TOC_]]

## Standalone Deployment

When started out with the online software stack, it is recommended to first use the standalone version. No special hardware is required, such as the Phase II processing cards, to get started. All aspects of the online software are run within docker containers on a single computer. This allows for you to gain an understanding of how all aspects of the software work together before deploying to real hardware.

The standalone stack contains three herd-dummy containers, which provide functional examples of all the endpoints on real HERD instances. At the Shep level, the stack has all the containers required to use Shep: the API server, a web-based UI, a database to store board information and a heartbeat server.

### Prerequisites
In order to get started with the standalone stack, the [docker](https://docs.docker.com/get-docker/) container runtime environment is required to be installed on a linux computer running the x86 architecture. If you are using a CentOS8 machine, please see the [installation instructions below](#docker-on-centos8).

Furthermore, [docker-compose](https://docs.docker.com/compose/install/) is also necessary for building the stack from the YAML file.

### Building the stack
The docker-compose YAML file for the standalone deployment can be found [here](deployments/standalone/docker-compose.yml). This file should either be downloaded or copied to a location on the local machine. The instructions for then starting the docker containers are as follows:
```shell
# First navigate to the directory containing the docker-compose.yml file
cd path-to-directory/

# Pull the images
docker-compose pull

# Start the containers
# (-d flag runs the containers in the background, omit this to see the logging output in the terminal)
docker-compose up -d
```
The shep web UI can then be found by opening a web browser and navigating to http://localhost/manager (or replace `localhost` with the computer's hostname/IP address if accessing from a different computer). To use a port other than the default for the HTTP interface, change the port mapping for the `nginx` container within the docker-compose YAML file and restart the containers by using the command `docker-compose up -d` from within the same directory as the YAML file.

Information on how to register boards and use the shep UI can be found [here](Shep User Guide.md).

The stack can be stopped by running the command `docker-compose down` from the directory containing the YAML file.

### Monitoring
A stack of [Loki](https://grafana.com/oss/loki/), [Promtail](https://grafana.com/docs/loki/latest/clients/promtail/) and [Grafana](https://grafana.com/oss/grafana/) is installed as part of the standalone example to serve as a monitoring solution. To be used, you need to navigate to http://localhost:3002 and login with the username `admin` and the password defined in `grafana_password.txt` (`P@ssw0rd` by default). From there you can navigate to Explore on the left to explore the logs of the system.

## HERD On Hardware + Shep Stack

The method for deploying the software a test stand with real processing cards is very similar to that of the standalone deployment described above. The difference being that no herd-dummy containers are run on the Shep computer and instead the HERD containers are run on the SoCs of the processing cards.

### Prerequisites
[Docker](https://docs.docker.com/get-docker/) must be installed on both the SoCs running the HERD instances as well as the computer runnig the Shep stack. If you are using a CentOS8 machine, please see the [installation instructions below](#docker-on-centos8).

Furthermore, [docker-compose](https://docs.docker.com/compose/install/) is also necessary for building the Shep stack from the YAML file, but is not required for the SoCs.

### Running HERD on the ATCA boards

The HERD application (implemented in the `herd-control-app` repository) is an executable that loads SWATCH/HERD plugins, and provides a network interface that allows remote applications to run the commands and FSM transitions procedures declared in those plugins. There are separate plugin libraries for different flavours of ATCA board, which are packaged up in docker containers using a common GitLab CI pipeline; the following sections describe how to run these containers.

#### Serenity

The Serenity plugin builds on the board-agnostic EMP plugin, and hence registers commands and FSMs both for board-independent EMP configuration procedures and for Serenity-specific procedures. If the HERD app is run with [example-config.yml](https://gitlab.cern.ch/p2-xware/software/serenity-herd/-/blob/master/example-config.yml) as the configuration file, the control application will load the Serenity plugin and create two devices: one for the Artix (named `artix`), the other (named `x1`) for a daughter card at the X1 site running EMP-based firmware.

The control application should typically be run in a CI-built docker container from the Serenity plugin repository; this is the simplest method for using the plugin, as it doesn't require building the code, simply downloading a docker container. Critical info:

  * The image *must* always be run using the `/opt/cactus/bin/serenity/docker-run.sh` script (which wraps `docker run`, adding extra arguments to e.g. make device files accessible inside the container).
  * Image URL: `gitlab-registry.cern.ch/p2-xware/software/serenity-herd/herd-app:v0.2.0`
  * Configuration
    * SMASH config file: Mount as `/board.smash`
    * HERD app config file: Either mount as `/herd-config.yml` or override by specifying the container `CMD`

Example command, using a non-default config file with path `/path/to/herd-config.yml` on the host machine, mounted as `/herd-config.yml` in the container:
   ```shell
   /opt/cactus/bin/serenity/docker-run.sh -d -p 3000:3000 -v /path/to/myBoard.smash:/board.smash -v /path/to/herd-config.yml:/herd-config.yml gitlab-registry.cern.ch/p2-xware/software/serenity-herd/herd-app:v0.2.0
   ``` 

#### Apollo

The Apollo-HERD plugin library registers Apollo-specific controls to the SWATCH framework by wrapping SWATCH code to call functions from the [ApolloSM_plugin](https://github.com/apollo-lhc/ApolloSM_plugin). If the HERD app is run with [Apollo.yml](https://gitlab.cern.ch/ammitra/apollo-herd/-/blob/master/Apollo.yml)
as the configuration file, the control application will load the SWATCH plugin and create three devices:

  * One device for the Service Module (named `ApolloSM_device`)
  * Two devices for the Command Module FPGAs (named `kintex` and `virtex`)

At the moment, the control application can be run with the Apollo plugin only in a docker container:

  * The image should *always* be run using the `start_apolloherd.sh` shell script. This script wraps the `docker run` command, makes all UIO devices accessible inside the container, mounts several volumes, and sets permissions.
  * There are different Docker images based on the architecture on which the container will run (`arm/v7` on the rev.1 blades and `arm64` on the rev.2 blades). However, one can use the multi-arch URL, also created by GitLab CI, and `docker` command will detect the architecture, and use the relevant image for the arch. The multi-arch URLs are shown below:
    * Tags: `gitlab-registry.cern.ch/ammitra/apollo-herd/herd-app:vX.Y.Z`
    * Branches: `gitlab-registry.cern.ch/ammitra/apollo-herd/herd-app:BRANCHNAME-COMMITSHA`  
      where COMMITSHA is the first 8 characters of the git commit's SHA

An example of running the plugin in a container using the non-default config file `/path/to/apollo-config/Apollo.yml` on the host machine, mounted as `/herd-config.yml` in the container is shown below:

  ```shell
  ./path/to/start_apolloherd.sh -d -p 3000:3000 -v /path/to/apollo-config/Apollo.yml:/herd-config.yml gitlab-registry.cern.ch/ammitra/apollo-herd/herd-app:master-55df6c8f
  ```

The above command:

1. Runs the container in the background with the -d flag
2. Maps TCP port 3000 in the container to 3000 on the Docker host
3. Mounts the configuration file `/path/to/apollo-config/Apollo.yml` on the host to `/herd-config.yml` in the container. Note that the desination file path **must** be specified as `/herd-config.yml` as the HERD plugin is looking for this config file as an input, in the root directory of the container.   
4. Specifies the appropriate apollo-herd image to run (with the specified image URL), and,
5. Starts the HERD control app with the appropriate Apollo-specific config file, `/herd-config.yml`

To run the plugin in a container with the default config file, the `-v` option could be ommitted:

```shell
./path/to/start_apolloherd.sh -d -p 3000:3000 gitlab-registry.cern.ch/ammitra/apollo-herd/herd-app:master-55df6c8f
```

Similar to the command above, this commmand will launch the HERD plugin with the default config file already contained within the specified image.

For debugging the image, you should run start_apolloherd.sh with the flag combination `-it --entrypoint bin/bash` in order to run the container interactively without the HERD control application starting up. Once inside the container, you can begin the plugin by running:

  ```shell
  source entrypoint_env.sh
  herd-control-app herd-config.yml
  ```

### Running Shep
The docker-compose YAML file for the shep stack can be found [here](deployments/shep-docker/docker-compose.yml). This file should either be downloaded or copied to a location on the local machine. The instructions for then starting the docker containers are as follows:
```shell
# First navigate to the directory containing the docker-compose.yml file
cd path-to-directory/

# Pull the images
docker-compose pull

# Start the containers (-d flag runs the containers in the background, omit this to see the logging output in the terminal)
docker-compose up -d
```
The shep web UI can then be found by opening a web browser and navigating to http://localhost/manager (or replace `localhost` with the computer's hostname/IP address if accessing from a different computer).

Information on how to register boards and use the shep UI can be found [here](Shep User Guide.md)


## Remote control from the command line

You can also remotely control a HERD application from the command line by running the console script. In particular, the command-line interface can be useful when developing plugins, as it allows you to quickly run commands without having to start up the full set of containers for the shep layer. This console and its dependencies are packaged in a docker container, so you run it as follows (replacing `BOARD_HOSTNAME` with your board's IP address or hostname):
```
docker run -it --network=host gitlab-registry.cern.ch/cms-tracker-phase2-onlinesw/herd-control-app/console:v0.2.0 BOARD_HOSTNAME 
```

Specifically, in this console you can:
 * list device, command and FSM information by typing `info`;
 * run commands and FSM transitions by typing `run DEVICE_ID COMMAND_OR_TRANSITION_ID` (e.g. `run x0 reset`);
 * engage FSMs by typing `engage-fsm DEVICE_ID FSM_ID` (e.g. `engage-fsm x0 myFSM`); 
 * reset FSMs by typing `reset-fsm DEVICE_ID` (e.g. `reset-fsm x0`); and
 * disengage FSMs by typing `disengage-fsm DEVICE_ID` (e.g. `disengage-fsm x0`).



## Docker on CentOS8

On RedHat 7, docker can be used out-of-the box by simply following the standard docker install instructions. On RedHat 8 - and derivatives like CentOS 8 - the default container runtime engine is Podman, and so in order for docker to work, you need to first run a dedicated command to install `containerd`, and also add a firewall masquerade. The following commands have been verified to work on a few computers:
```
# Install docker
sudo dnf install -y https://download.docker.com/linux/centos/7/x86_64/stable/Packages/containerd.io-1.2.13-3.2.el7.x86_64.rpm
sudo dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo sed -i 's|\$releasever|7|g' /etc/yum.repos.d/docker-ce.repo 
sudo dnf install -y docker-ce-19.03.11 docker-ce-cli-19.03.11
sudo dnf install -y https://download.docker.com/linux/centos/7/x86_64/stable/Packages/containerd.io-1.2.13-3.2.el7.x86_64.rpm
sudo dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo sed -i 's|\$releasever|7|g' /etc/yum.repos.d/docker-ce.repo 
sudo dnf install -y docker-ce-19.03.11 docker-ce-cli-19.03.11

# Add firewall masquerade, then restart docker
sudo firewall-cmd --zone=public --add-masquerade --permanent
sudo firewall-cmd --reload
sudo systemctl restart docker
sudo systemctl is-active docker

# Verify that DNS working in containers
docker run centos:8 getent hosts google.com
```


## Project links

| Project |
|---------|
| [herd-control-app](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/herd-control-app/) |
| [herd-dummy](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/herd-dummy/) |
| [shep-api-server](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/shep-api-server/) |
| [shep-ui](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/shep-ui/) |
