# Kubernetes
For large scale deployments, the recommended method is a Kubernetes cluster. The cluster should be setup such that all processing cards are nodes within the cluster as well as sufficient server nodes to host the shep level pods.

This section is still under active development and should **not** be used by non-developers.

# Environment
To run this setup a running kubernetes cluster is required. For local development [K3D](https://k3d.io/) is recommended and assumed for the rest of this guide. 

## Helm Setup
[Helm](https://helm.sh) is required for installing Loki.
```bash
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
```

## Cluster Setup
The following commands create a k3d cluster with a master and worker node.
```bash
k3d cluster create demo -a 2
```

The following command updates kubectl to direct future commands to this new cluster.
```bash
kubectl config use-context k3d-demo
```

To test the daemonset aproach, one of the nodes must be labeled and tainted so that other pods don't execute on that node.
```bash
kubectl label nodes k3d-demo-agent-1 role=device
kubectl taint nodes k3d-demo-agent-1 role=device:NoSchedule
```

# Storage
For the local deployment via K3D, this section is not relevant as it already provides a default storage option. However, if you are deploying this on a production system a number of components within benefit from having a distributed persistent storage option available.  
To facilitate this, [Longhorn](https://longhorn.io/) configurations are included in this repository.

To install Longhorn the following commands are required:
```bash
helm repo add longhorn https://charts.longhorn.io
helm repo update
helm install longhorn longhorn/longhorn --namespace longhorn-system --create-namespace --values storage/longhorn/values.yaml
```

Once Longhorn has been installed, two configuration files have to be updated to tell the persistant storage requests to use it. These are `loki/values.yaml` and `shep/api/database/deployment.yaml` and they both have the line `storageClassName: local-path` which needs to be changed to `storageClassName: longhorn`.

# Deployment
Running the following command from the location of `kustomization.yaml` will install the entire system.

```bash
kubectl apply -k .
```

The following command will wait until the system is up and running.
```bash
kubectl wait --for=condition=Ready pod -l "app in (shep-database,shep-api,shep-ui,shep-heartbeat,herd-dummy)"
```

## Monitoring
```bash
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update

helm upgrade --install loki grafana/loki -f loki/values.yaml
kubectl apply -f loki/datasource.yaml

helm upgrade --install promtail grafana/promtail -f promtail/values.yaml

helm upgrade --install cms grafana/grafana -f grafana/values.yaml
```

### Grafana
To extract the password.
```bash
kubectl get secret --namespace default cms-grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo
```

To port forward from kubernetes to localhost.
```bash
export POD_NAME=$(kubectl get pods --namespace default -l "app.kubernetes.io/name=grafana,app.kubernetes.io/instance=cms" -o jsonpath="{.items[0].metadata.name}")
kubectl --namespace default port-forward $POD_NAME 3000
```

To access it you need to go to http://localhost:3000 and log in with the user `admin` and the generated password.

# Interacting with the SHEP UI
The following command will return the IP address used to access the SHEP UI via a web browser.
```bash
echo `kubectl get -n kube-system svc/traefik -o jsonpath="{.status.loadBalancer.ingress[0].ip}"
```
